
#include "vector_util.h"
#include <iostream>


void print_vector(const mvec::Vector& vec)
{
  std::cout << '(' << vec.get_x() << ", " << vec.get_y() << ")\n";
}


mvec::Vector get_zero_vector()
{
  return mvec::Vector{ 0., 0.};   // We create an anonymous object
}
