
#ifndef VECTOR_H
#define VECTOR_H

namespace mvec
{  
  class Vector
  {
  private:
    double m_x {};
    double m_y {};

  public:
    Vector(double x, double y);
  
    Vector add(const Vector& v);
    double get_x() const { return m_x; };
    double get_y() const { return m_y; };
  };
}

#endif
