
#ifndef VECTOR_UTIL_H
#define VECTOR_UTIL_H

#include "Vector.h"


void print_vector(const mvec::Vector& vec);

mvec::Vector get_zero_vector();


#endif
