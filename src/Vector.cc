
#include "Vector.h"

using namespace mvec;


Vector::Vector(double x, double y)
  : m_x{ x }, m_y{ y }
{
  // empty
}


Vector Vector::add(const Vector& v)
{
  // Returning an anonymous object.
  return Vector{ m_x + v.m_x, m_y + v.m_y };
}

