
#include "Vector.h"
#include "vector_util.h"
#include <iostream>

int main()
{
  std::cout << "Testing Vector class and vector_util.\n";
  mvec::Vector vector{ 1., 1.};
  auto zero_vector{ get_zero_vector() };

  print_vector(vector);
  print_vector(zero_vector);

  auto sum{ vector.add(zero_vector) };

  print_vector(sum);

  std::cout << "That's it!\n";
}
