
# My Elemental cmake Project

This is a simple to use cmake project that containe the basic parts of a cmake
project.

Directories
-----------
src - Source folder for main files. Main files will be compiled to a library
      and linked to an executable defined in main.cc.

testing - Directory for testing executables. Here, testing scripts can be
	  written and added as a test with ctest. Tests are ran with
	  $ make test

build - Build folder where all executables are built.

The program is built from the build folder using

$ cmake -S ../ -B .

or alternatively

$ cmake -S ../

